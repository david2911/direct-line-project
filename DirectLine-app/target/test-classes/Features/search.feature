@regression
Feature: Volkswagen Financial Services Vehicle Insurance
  As a vehicle owner
  I want to see if vehicle exist
  So i can verify cover dates

  Background:
    Given I am on the HomePage
    When I enter the "vehicle.registration.number"

  Scenario: Verify Vehicle Registration Number Cover
    Then I should be able to verify the "result"

  Scenario: Insurance Cover Dates
    Then I should be able to verify the cover dates


