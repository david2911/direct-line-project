package com.directline.www.pages;

import com.directline.www.utilities.EnvConfig;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class BasePage {
    protected final WebDriver webDriver;

    public BasePage(WebDriver driver) {
        this.webDriver = driver;
    }

    public HomePage loadApp(){
        webDriver.navigate().to(EnvConfig.getValue("base.url"));
        return PageFactory.initElements(webDriver, HomePage.class);
    }
}
