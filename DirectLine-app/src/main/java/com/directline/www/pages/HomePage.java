package com.directline.www.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BasePage {
    public HomePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "vehicleReg")
    private WebElement vehicleRegElement;

    @FindBy(id = "search")
    private WebElement searchElement;

    public HomePage searchWithRegNum(String regNum) {
        vehicleRegElement.sendKeys(regNum);
        searchElement.click();
        return PageFactory.initElements(webDriver, HomePage.class);
    }

    public boolean validateResult(String result) {
        return webDriver.getTitle().contains(result);
    }

    public boolean verifyCoverDates() throws InterruptedException {
        Thread.sleep(3000);
        return webDriver.getPageSource().contains("09 FEB 2022 : 16 : 26")
                && webDriver.getPageSource().contains(" 18 FEB 2022 : 23 : 59");
    }
}
