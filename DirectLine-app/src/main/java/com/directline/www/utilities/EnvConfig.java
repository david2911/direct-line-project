package com.directline.www.utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class EnvConfig {

    static String env = System.getProperty("env");

    public static String getValue(String keyName){

        String configPath = "//src//main//resources//envs//"+env+".properties";
        String sysPath = System.getProperty("user.dir")+ configPath;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(sysPath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Properties properties = new Properties();
        try {
            properties.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties.getProperty(keyName);
    }

    public static String getTData(String keyName){
        String configPath = "//src//main//resources//testData//"+env+ "_testdata.properties";
        String sysPath = System.getProperty("user.dir")+configPath;
        FileInputStream fis = null;
        try {
             fis = new FileInputStream(sysPath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Properties properties = new Properties();
        try {
            properties.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties.getProperty(keyName);
    }
}
