package com.directline.www.browsers;

import com.directline.www.utilities.EnvConfig;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class LocalBrowserManager {

    private WebDriver driver;

    public WebDriver createLocalBrowser(){
        String browserName = EnvConfig.getValue("browser.name").toLowerCase();
        if (browserName.contains("chrome")){
            createChromeBrowser();
        }else if (browserName.contains("firefox")){
            createFirefoxBrowser();
        }
        return driver;
    }

    private void createFirefoxBrowser() {
        WebDriverManager.firefoxdriver().setup();
        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("--headless");
        options.addArguments("--incognito");
        options.addArguments("--marionette");
        driver = new FirefoxDriver(options);
    }

    private void createChromeBrowser() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
//        options.addArguments("--headless");
        options.addArguments("--incognito");
        options.addArguments("--disable-infobars");
        options.addArguments("--enable-javascript");
        options.addArguments("--disable-websecurity");
        driver = new ChromeDriver(options);
    }
}
