package com.directline.www.browsers;

import com.directline.www.utilities.EnvConfig;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class WebDriverFactory {

    private static WebDriver driver;

    public static WebDriver getDriver() {
        return driver;
    }


    public WebDriver initialiseBrowser() {
        String placeOfExecution = EnvConfig.getValue("place.execution").toLowerCase();
        if (placeOfExecution.equalsIgnoreCase("local")) {
            LocalBrowserManager lbm = new LocalBrowserManager();
            driver = lbm.createLocalBrowser();
        }
        return driver;
    }

    public void prepareBrowser() {
        driver.manage().deleteAllCookies();
        driver.navigate().refresh();
        driver.manage().window().maximize();
        long timeout = Long.parseLong(EnvConfig.getValue("global.time.out"));
        driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.MICROSECONDS);
    }

    public void deInitialiseBrowser(){
        if (driver != null){
            driver.quit();
        }
    }
}
