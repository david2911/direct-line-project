package com.directline.www.hooks;

import com.directline.www.browsers.WebDriverFactory;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class HookSteps {
    private WebDriverFactory webDriverFactory;

    @Before(order = 1)
    public void startTest1(){
        webDriverFactory = new WebDriverFactory();
        webDriverFactory.initialiseBrowser();
    }

    @Before(order = 2)
    public void startTest2(){
        webDriverFactory.prepareBrowser();
    }

    @After(order = 3)
    public void stopTest(){
        webDriverFactory.deInitialiseBrowser();
    }
}
