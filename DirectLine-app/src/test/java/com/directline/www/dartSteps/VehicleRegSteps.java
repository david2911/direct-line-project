package com.directline.www.dartSteps;

import com.directline.www.browsers.WebDriverFactory;
import com.directline.www.pages.BasePage;
import com.directline.www.pages.HomePage;
import com.directline.www.utilities.EnvConfig;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.hamcrest.core.Is.*;
import static org.junit.Assert.assertThat;

public class VehicleRegSteps {

    private HomePage homePage;

    @Given("^I am on the HomePage$")
    public void iAmOnTheHomePage() throws Throwable {
        BasePage basePage = new BasePage(WebDriverFactory.getDriver());
        homePage = basePage.loadApp();
    }

    @When("^I enter the \"([^\"]*)\"$")
    public void iEnterThe(String regNum) throws Throwable {
        homePage = homePage.searchWithRegNum(EnvConfig.getTData(regNum));
    }

    @Then("^I should be able to verify the \"([^\"]*)\"$")
    public void iShouldBeAbleToVerifyThe(String result) throws Throwable {
        assertThat(homePage.validateResult(EnvConfig.getTData(result)), is(true));
    }

    @Then("^I should be able to verify the cover dates$")
    public void iShouldBeAbleToVerifyTheCoverDates() throws Throwable {
        assertThat("Cover dates are not present", homePage.verifyCoverDates(), is(true));
    }
}
